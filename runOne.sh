#!/bin/bash

unamestr=`uname`

rm Istim.dat hodgkinOneCell V1.dat
if [[ "$unamestr" == 'Linux' ]]; then
        gcc -o hodgkinOneCell hodgkinOneCell.c -lm
elif [[ "$unamestr" == 'Darwin' ]]; then
        gcc-8 -o hodgkinOneCell hodgkinOneCell.c -lm
fi

./hodgkinOneCell

gnuplot << EOG
set term pngcairo size 800,600 enhanced
set output 'one.png'
set title "single HH cell"
set ytics nomirror
set yrange [-100:60]
set xlabel "time [ms]"
set ylabel "membrain potential [mv]"
plot 'Istim.dat' u 1:3 w l title 'membrain potential'
EOG

echo "Average firing rate: `awk '{sum+=$1} END {print sum/NR}' FR.dat`"
echo "STD of firing rate: `awk '{sum+=$1; sumsq+=$1*$1} END {print sqrt(sumsq/NR - (sum/NR)**2)}' FR.dat`"

if [[ "$unamestr" == 'Linux' ]]; then
        eog one.png
elif [[ "$unamestr" == 'Darwin' ]]; then
        open one.png
fi
