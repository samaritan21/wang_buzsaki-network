set term svg enhanced size 320,500 font "Arila,9"
set output 'Hodgkin.svg'
set size square
set style line 1 lc rgb '#0025ad' lt 2 lw 1 pt 7 ps .5
set style line 2 lc rgb '#00ad6b' lt 2 lw 1 pt 7 ps .5
set style line 3 lc rgb '#80ad00' lt 2 lw 1 pt 7 ps .5
set style line 4 lc rgb '#000000' lt 1 lw 1 pt 7 ps .5
set multiplot layout 2,1 rowsfirst
set tmargin at screen 0.90; set bmargin at screen 0.60
set yrange[-20:120]; set ylabel 'membrane potential (mv)'
set xrange[0:*]; set xlabel 'time (s)'
p 'Istim.dat' u 1:3 notitle with lines ls 4
set tmargin at screen 0.50; set bmargin at screen 0.20
set yrange[0:1]; set ylabel 'gating variables'
set xrange[0:*]; set xlabel 'time (s)'
p 'Istim.dat' u 1:4 title 'm' with lines ls 1, \
    'Istim.dat' u 1:5 title 'n' with lines ls 2, \
    'Istim.dat' u 1:6 title 'h' with lines ls 3
unset multiplot
