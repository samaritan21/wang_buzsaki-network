# Wang_Buzsaki Network

A neural network of Hodgkin-Huxley cells with GABAa receptor types defined by 
Wang/Buzsaki:  
Gamma Oscillation by Synaptic Inhibition in a Hippocampal Interneuronal Network 
Model  
Xiao-Jing Wang, György Buzsáki
Journal of Neuroscience 15 October 1996, 16 (20) 6402-6413; 
DOI: 10.1523/JNEUROSCI.16-20-06402.1996  

All the differential equations are solved by the fourth order Runge–Kutta 
method.  

# Requirements
The c-codes of this project can be compiled by gcc in any Unix-based machine. 
The results are stored to disk as text data-files. These datafiles are then 
plotted using gnuplot. Power spectrum of the mean activity of the network 
is calculated via a Python code (`spectrum.py`)

In case of the single cell or network model, the accompanying bash script 
compiles the code, runs the simulation, plots the results and displays them 
in a gmone-based Linux or a Mac OS.

## Single cell code

`hodgkinOneCell.c` simulates the membrane potential of one Wang-Buzsaki cell.  
All the input parameters are read from the input file `hodgkinOneCell.in`.  
To run the code and plot membrane potential as a function of time, run 
`runOne.sh`.  
### Results:
`one.png` -> Membrane potential [mv] as a function of time [ms].

## Neural network code

`hodgkin.c` simulates the spiking activity of anetwork of  Wang-Buzsaki cells.  
All the input parameters are read from the input file `hodgkin.in`.  
To run the code and plot membrane potential as a function of time, run 
`runCode.sh`.  
### Results:
`sample.png` -> Membrane potential [mv] of a sample cell as a function 
of time [ms].  
`synaptic_field.png` -> Average synaptic inputs in the network [au] as a 
function of time [ms]  
`coherence.png` -> Network coherence [au] as a function of sampling time 
window size [ms]  
`raster.png` -> Raster plot of the spikes in the network as a function of time 
[ms]  
`spectrum.png` -> power spectrum of the network oscillation.