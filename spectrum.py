# -*- coding: utf-8 -*-
"""
Created on Fri Aug 19 10:57:59 2016

@author: aziziart
"""

import numpy as np
import matplotlib.pyplot as plt

a = np.loadtxt("lfp.dat")
opto_duration=[600.,700.]
b=(a[:,0]>opto_duration[0]) & (a[:,0]<opto_duration[1])
bb=(a[:,0]>opto_duration[1]) & (a[:,0]<(opto_duration[1]+100.))

data = a[b,1]
data2 = a[bb,1]
times = a[b,0]
times2 = a[bb,0]
time_step = np.max(np.diff(times,n=1))*.001

rate = 1./time_step
print(rate)

fig = plt.figure()

p = .02*np.log10(np.abs(np.fft.rfft(data))**2)
N = len(p)
p2 = .02*np.log10(np.abs(np.fft.rfft(data2))**2)
N2 = len(p2)
f = np.linspace(0, rate/2., N)
f2 = np.linspace(0, rate/2., N2)
plt.plot(f, p**2, 'k-', label="optogenetic")
plt.plot(f, p2**2, 'k--', label="baseline")
plt.legend( loc='best',numpoints=1,prop={'size':14})
plt.xlim(0,500)
plt.xlabel("frequency (Hz)")
plt.ylabel("power spectrum of s(t)")
plt.savefig("spectrum.png", format='png', dpi=600)
plt.show()
