/* Hodgkin-Huxley Model of Neuron's Membrane Potential */
/*                                                     */
/*              Amir Hossein Azizi                     */
/*                                                     */
/*    <Dynamics of Neurons> Winter 2016, RUB           */


#include "hh_defs.h"

void AddList(List *, real);
void AddList2(List2 *, int);
void AllocArrays(int);
void Analysis(int);
void Coherence(int);
void InitList(void);
int InRand(int);
real Kh(real, real);
real Kn(real, real);
real KV(real, real, real, real, real, real);
real Ks(real, real);
void SetParams(void);
void SingleStep(void);
void ComputeCurrents(void);
void PrintAnalysis(void);
void PrintFiringRates(void);
void PrintRaster(void);
void PrintSummary(FILE *);
real rand_gauss(void);
real RandR(void);
void PrintProgress(real);

typedef struct {
  real v;			//membrain potential
  real m, n, h;			//activation variables
  real iNa, iK, iL;		//cell currents
  real stim, iSyn, iSynO;	//external and synaptic currents
  real stimTime;		//random start of the stimulation time
  int isOn;			//1 if v>spikeThre
  List spike;			//spike train of the cell
  List2 pre;			//indices of pre-synaptic cells
  List s;			//fraction of open synaptic ion channels per synapse
} NeuronI;

NeuronI *neti;


NameList nameList[] = {
  NameI (nEx),
  NameI (nIn),
  NameI (selected),
  NameR (Capacity),
  NameR (gNaBar),
  NameR (gKBar),
  NameR (gLBar),
  NameR (gSynBar),
  NameR (eNa),
  NameR (eK),
  NameR (eL),
  NameR (eSyn),
  NameR (phi),
  NameI (mSyn),
  NameR (vRest),
  NameR (tMax),
  NameR (deltaT),
  NameR (delay),
  NameI (stepAvg),
  NameR (iMu),
  NameR (iSig),
  NameR (optoStim),
  NameR (stimStart),
  NameR (duration),
  NameR (binSize),
  NameR (spikeThre),
  NameR (alpha),
  NameR (beta),
};

real *synDrive, *initialCurrent;
int *perm;

int main(int argc, char **argv) {

  int ij;
  GetNameList (argc, argv);
  AllocArrays(0);
  InitList();
  SetParams();
  printf("number of synapses perneuron = %d, %d\n", neti[2].s.len, neti[2].pre.len);
  printf("max time steps = %f\n", tMax/deltaT);
  moreCycles = 1;
  real progress=0.;
  real incre=deltaT/tMax;
  while (moreCycles) {
    SingleStep();
    if (stepCount >= (tMax / deltaT)) moreCycles = 0;
    PrintProgress(progress);
    progress += incre;
  }
  PrintAnalysis();
  for(ij=1.; ij<binSize; ij+=1.) {
    tau = ij;
    hh += 1;
    Coherence(hh);
  }
  PrintFiringRates();
  PrintRaster();
  AllocArrays(1);
}

void SingleStep() {
  int k, l;
  ++stepCount;
  timeNow = stepCount * deltaT;
  DO_INH {
    if ((timeNow >= neti[k].stimTime) && (timeNow <= (duration + neti[k].stimTime))) {
      neti[k].stim = initialCurrent[k];//0.;iMu + iSig*rand_gauss();
    } else {
      neti[k].stim = 0.;//initialCurrent[k];
    }
  }
  if(timeNow>600. && timeNow<=700.) {
    for(l=0; l<selected; l++)
      neti[perm[l]].stim = iMu+optoStim;
  }
  ComputeCurrents();
  Analysis(stepCount-1);
//  if(stepCount % stepAvg ==0) {
//  }
//  PrintSummary(fopen("Istim.dat", "a+"));
}

void SetParams() {
  int k, l, j, td;
  real P;
  P = ((real) mSyn)/((real) nIn);
  gSynBar /= mSyn;
  printf("G_syn=%g, P=%g\n", gSynBar, P);
  oldTime = 0.;
  DO_INH {
    neti[k].v = -1.*(50.+20.*RandR());
    neti[k].m = AlphaM(vRest) / (AlphaM(vRest) + BethaM(vRest));
    neti[k].h = AlphaH(vRest) / (AlphaH(vRest) + BethaH(vRest));
    neti[k].n = AlphaN(vRest) / (AlphaN(vRest) + BethaN(vRest));
    initialCurrent[k] = iMu + iSig*rand_gauss();
    neti[k].stimTime = stimStart + 0.*RandR();
    neti[k].iSyn = 0.;
    neti[k].iSynO = 0.;
    neti[k].isOn = 1;
    for(l=0; l<nIn; l++) {
      if(RandR()<P && k!=l) {
	AddList2(&neti[k].pre, l);
	AddList(&neti[k].s, 0.);
      }
    }
    perm[k] = k;
  }
  DO_INH {
    j = InRand(nIn-k)+k;
    td = perm[k];
    perm[k] = perm[j];
    perm[j] = td;
  }
  printf("network setup done!\n");
}

void AllocArrays(int domem) {
  switch(domem) {
    case 0:
      AllocMem(neti, nIn, NeuronI);
      AllocMem(synDrive, (long) (tMax/deltaT), real);
      AllocMem(initialCurrent, nIn, real);
      AllocMem(perm, nIn, int);
      break;
    case 1:
      free(neti);
      free(synDrive);
      free(initialCurrent);
      free(perm);
      break;
  }
}

void InitList() {
  int k, l;
//  DO_EXC {
//  }
  DO_INH {
    neti[k].spike.len = 0;
    neti[k].spike.index = NULL;
    neti[k].pre.len = 0;
    neti[k].pre.index = NULL;
    neti[k].s.len = 0;
    neti[k].s.index = NULL;
  }
}

void ComputeCurrents() {
  int k, l;
  real *k11, *k12, *k13, *k14, *h11, *h12, *h13, *h14, *n11, *n12, *n13, *n14, s11, s12, s13, s14, *totS;
  AllocMem(totS, nIn, real);
  AllocMem(k11, nIn, real);
  AllocMem(k12, nIn, real);
  AllocMem(k13, nIn, real);
  AllocMem(k14, nIn, real);
  AllocMem(h11, nIn, real);
  AllocMem(h12, nIn, real);
  AllocMem(h13, nIn, real);
  AllocMem(h14, nIn, real);
  AllocMem(n11, nIn, real);
  AllocMem(n12, nIn, real);
  AllocMem(n13, nIn, real);
  AllocMem(n14, nIn, real);
  DO_INH {
    k11[k] = deltaT*KV(neti[k].iSynO,neti[k].stim,neti[k].v,neti[k].m,neti[k].h,neti[k].n);
    h11[k] = deltaT*Kh(neti[k].v,neti[k].h);
    n11[k] = deltaT*Kn(neti[k].v,neti[k].n);

    k12[k] = deltaT*KV(neti[k].iSynO,neti[k].stim,neti[k].v+(.5*k11[k]),neti[k].m,neti[k].h+(.5*h11[k]),neti[k].n+(.5*n11[k]));
    h12[k] = deltaT*Kh(neti[k].v+(.5*k11[k]),neti[k].h+(.5*h11[k]));
    n12[k] = deltaT*Kn(neti[k].v+(.5*k11[k]),neti[k].n+(.5*n11[k]));

    k13[k] = deltaT*KV(neti[k].iSynO,neti[k].stim,neti[k].v+(.5*k12[k]),neti[k].m,neti[k].h+(.5*h12[k]),neti[k].n+(.5*n12[k]));
    h13[k] = deltaT*Kh(neti[k].v+(.5*k12[k]),neti[k].h+(.5*h12[k]));
    n13[k] = deltaT*Kn(neti[k].v+(.5*k12[k]),neti[k].n+(.5*n12[k]));

    k14[k] = deltaT*KV(neti[k].iSynO,neti[k].stim,neti[k].v+k13[k],neti[k].m,neti[k].h+h13[k],neti[k].n+n13[k]);
    h14[k] = deltaT*Kh(neti[k].v+k13[k],neti[k].h+h13[k]);
    n14[k] = deltaT*Kn(neti[k].v+k13[k],neti[k].n+n13[k]);

    neti[k].m = AlphaM(neti[k].v) / (AlphaM(neti[k].v) + BethaM(neti[k].v));
    neti[k].v = neti[k].v + ((k11[k] + 2.*k12[k] + 2.*k13[k] + k14[k])/6.);
    neti[k].h = neti[k].h + ((h11[k] + 2.*h12[k] + 2.*h13[k] + h14[k])/6.);
    neti[k].n = neti[k].n + ((n11[k] + 2.*n12[k] + 2.*n13[k] + n14[k])/6.);

    if(neti[k].v>=spikeThre && neti[k].isOn==1) {
      AddList(&neti[k].spike, timeNow);
      neti[k].isOn = 0;
//      printf("%d ", k);
    }
    if(neti[k].v<spikeThre)
      neti[k].isOn = 1;

  }
  DO_INH {
//    totS[k] = 0.;
    neti[k].iSyn = 0.;
    for(l=0; l<neti[k].s.len; l++) {
      s11 = deltaT*Ks(neti[neti[k].pre.index[l]].v,neti[k].s.index[l]);
      s12 = deltaT*Ks(neti[neti[k].pre.index[l]].v+(.5*k11[k]),neti[k].s.index[l]+(.5*s11));
      s13 = deltaT*Ks(neti[neti[k].pre.index[l]].v+(.5*k12[k]),neti[k].s.index[l]+(.5*s12));
      s14 = deltaT*Ks(neti[neti[k].pre.index[l]].v+k13[k],neti[k].s.index[l]+s13);
      neti[k].s.index[l] += ((s11 + 2.*s12 + 2.*s13 + s14)/6.);
      neti[k].iSyn += gSynBar * neti[k].s.index[l] * (neti[k].v - eSyn);
//      totS[k] += neti[k].s.index[l];
    }
  }
  if(abs(timeNow-(oldTime+delay))<=deltaT) {
    oldTime = timeNow;
    DO_INH
      neti[k].iSynO = neti[k].iSyn;
  }
  free(totS);
  free(k11);
  free(k12);
  free(k13);
  free(k14);
  free(h11);
  free(h12);
  free(h13);
  free(h14);
  free(n11);
  free(n12);
  free(n13);
  free(n14);
}

real KV(real Isyn, real I, real V, real m, real h, real n) {
  real p;
  p = (I - ((gNaBar*(m*m*m)*h*(V-eNa)) + (gKBar*(n*n*n*n)*(V-eK)) + (gLBar*(V-eL)) + (Isyn)))/Capacity;
  return (p);
}

real Kh(real V, real h) {
  real p;
  p = phi * ((AlphaH(V) * (1. - h)) - (BethaH(V) * h));
  return (p);
}

real Kn(real V, real n) {
  real p;
  p = phi * ((AlphaN(V) * (1. - n)) - (BethaN(V) * n));
  return (p);
}

real Ks(real V, real s) {
  real p;
  p = alpha * ((F(V) * (1. - s)) - (beta * s));
  return (p);
}

void AddList(List *list,  real incre) {
  list->index=(real *)realloc(list->index, sizeof(real)*(list->len+1));
  list->index[list->len]=incre;
  list->len++;
}

void AddList2(List2 *list,  int incre) {
  list->index=(int *)realloc(list->index, sizeof(int)*(list->len+1));
  list->index[list->len]=incre;
  list->len++;
}

void PrintSummary(FILE *fp) {
  fprintf(fp, "%8.4f %7.4f %8.4f %8.4f %8.4f %8.4f %7.4f %8.4f %8.4f %8.4f %8.4f\n", timeNow, neti[perm[0]].stim, neti[perm[0]].v, neti[perm[0]].m, neti[perm[0]].n, neti[perm[0]].h, neti[11].stim, neti[11].v, neti[11].m, neti[11].n, neti[11].h);
  fflush(fp);
  fclose(fp);
}

void Analysis(int tt) {
  int k, l;
  real dummy;
  synDrive[tt] = 0.;
  DO_INH {
    dummy = 0.;
    for(l=0; l<neti[k].s.len; l++)
      dummy += neti[k].s.index[l];
    synDrive[tt] += (1./((real) nIn)) * dummy;
  }
}

void Coherence(int ht) {
  int k, totBins, i, j, pairNums, b, counter, c, dumCount;
  real l, *kappa, numen, denum1, denum2;
  typedef struct {
    real ind[(long) ((tMax-100.)/tau)];
  } train;
  train *spTrain;
  totBins = tMax/tau;
  pairNums = nIn*(nIn-1)/2;
  printf("\ncalculating the coherence measure...\n");
  AllocMem(spTrain, nIn, train);
  DO_INH {
    counter = 0;
    for(l=tMax-100.; l<tMax; l+=tau) {
      for(b=0; b<neti[k].spike.len; b++) {
	spTrain[k].ind[counter] = 0.;
	if(neti[k].spike.index[b]>=l && neti[k].spike.index[b]<=(l+tau)) {
	  spTrain[k].ind[counter] = 1.;
	  break;
	}
      }
      counter += 1;
    }
  }
  AllocMem(kappa, pairNums, real);
  dumCount = 0;
  char buff[30];
  sprintf(buff, "%s_%05d", "coherence", ht);
  strcat(buff, ".dat");
  FILE *flo;
  flo = fopen(buff, "w+");
  for(i=0; i<(nIn-1); i++) {
    for(j=(i+1); j<nIn; j++) {
      numen = 0.;
      denum1 = 0.;
      denum2 = 0.;
      for(c=0; c<counter-1; c++) {
	numen += spTrain[i].ind[c]*spTrain[j].ind[c];
	denum1 += spTrain[i].ind[c];
	denum2 += spTrain[j].ind[c];
      }
      kappa[dumCount] = numen/sqrt(denum1*denum2);
      if(!isnan(kappa[dumCount]))
	fprintf(flo, "%g\n", kappa[dumCount]);
      dumCount += 1;
    }
  }
  fclose(flo);
  free(spTrain);
  free(kappa);
}

void PrintAnalysis() {
  int i;
  FILE *flp;
  flp = fopen("lfp.dat", "w+");
  for(i=0; i<((long) (tMax/deltaT)); i++)
    fprintf(flp, "%f %f\n", i*deltaT, synDrive[i]);
  fclose(flp);
}

void PrintFiringRates() {
  int k, i;
  FILE * fll;
  fll = fopen("FR.dat", "w+");
  DO_INH {
    fprintf(fll, "%g\n", (neti[k].spike.len/tMax)*1000.);
  }
  fclose(fll);
}

void PrintRaster() {
  int k, i;
  FILE *fp, *flf, *fff, *fin;
  fp = fopen("V21.dat", "w+");
  flf = fopen("V1.dat", "w+");
  fff = fopen("indexes1.dat", "w+");
  fin = fopen("stimInd.dat", "w+");
  DO_INH {
    for(i=0; i<neti[k].spike.len; i++) {
      fprintf(fp, "%g ", neti[k].spike.index[i]);
      fprintf(flf, "%d %g\n", k, neti[k].spike.index[i]);
    }
    fprintf(fp, "\n");
    fprintf(fff, "%d\n", neti[k].spike.len);
    fprintf(fin, "%d\n", perm[k]+1);
  }
  fclose(fp);
  fclose(flf);
  fclose(fff);
  fclose(fin);
}

void PrintProgress(real percentage) {
  int val = (int) (percentage * 100);
  int lpad = (int) (percentage * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
  fflush(stdout);
}

#include "in_namelist.c"
#include "in_rand.c"
