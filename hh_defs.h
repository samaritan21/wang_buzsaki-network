#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef double real;
typedef struct {real x, y;} VecR;

#include "in_namelist.h"

typedef struct {
  real *index;
  int len;
} List;

typedef struct {
  int *index;
  int len;
} List2;

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

#define AllocMem(a, n, t) a = (t *) malloc ((n) * sizeof (t))

int nEx, nIn, selected, stepCount = 0, moreCycles, mSyn, hh=0, stepAvg;
real gNaBar, gKBar, gLBar, gSynBar, eNa, eK, eL, eSyn, phi, timeNow, Capacity;
real vRest, deltaT, iMu, iSig, optoStim, stimStart, duration, binSize, tau, stim, tMax, spikeThre, alpha, beta, oldTime, delay;


#define DO_EXC for(l=0; l<nEx; l++)
#define DO_INH for(k=0; k<nIn; k++)

#define AlphaM(x)  -0.1 * (((x) + 35.) / (exp(-.1*((x) + 35.) ) - 1.))
//#define AlphaM(x)  ((.1*(x+35.))<(1e-6) ? (1.+(x+35.)/20.) : (-0.1 * ((x + 35.) / (exp(-.1*(x + 35.) ) - 1.))))
#define AlphaN(x)  -0.01 * (((x)+34.) / (exp(-.1*((x)+34.)) - 1.))
//#define AlphaN(x)  ((.1*(x+34.))<(1e-6) ? (.1*(1.+(x+34.)/20.)) : (-0.01 * ((x+34.) / (exp(-.1*(x+34.)) - 1.))))
#define AlphaH(x)  0.07 * exp(-1.*(x+58.) / 20.)
#define BethaM(x)  4. * exp(-1.*(x+60.) / 18.)
#define BethaN(x)  0.125 * exp(-1.*(x+44.) / 80.)
#define BethaH(x)  1. / (exp(-.1*(x+28.)) + 1.)
#define F(x)       1./(1. + exp((-1.*x)/2.))
