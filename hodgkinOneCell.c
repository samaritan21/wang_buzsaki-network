/* Hodgkin-Huxley Model of Neuron's Membrane Potential */
/*                                                     */
/*              Amir Hossein Azizi                     */
/*                                                     */
/*    <Dynamics of Neurons> Winter 2009, IASBS         */


#include "hh_defs.h"

void AddList(List *, real);
void AddList2(List2 *, int);
void AllocArrays(int);
void Coherence(int);
void InitList(void);
real Kh(real, real);
real Kn(real, real);
real KV(real, real, real, real, real);
void SetParams(void);
void SingleStep(void);
void ComputeCurrents(void);
void PrintFiringRates(void);
void PrintRaster(void);
void PrintSummary(FILE *);
real rand_gauss(void);
real RandR(void);

typedef struct {
  real v;			//membrain potential
  real m, n, h;			//activation variables
  real iNa, iK, iL;		//cell currents
  real stim;			//external and synaptic currents
  int isOn;			//1 if v>spikeThre
  List spike;			//spike train of the cell
} NeuronI;

NeuronI *neti;


NameList nameList[] = {
  NameI (nEx),
  NameI (nIn),
  NameR (Capacity),
  NameR (gNaBar),
  NameR (gKBar),
  NameR (gLBar),
  NameR (gSynBar),
  NameR (eNa),
  NameR (eK),
  NameR (eL),
  NameR (eSyn),
  NameR (phi),
  NameI (mSyn),
  NameR (vRest),
  NameR (tMax),
  NameR (deltaT),
  NameR (delay),
  NameI (stepAvg),
  NameR (iMu),
  NameR (iSig),
  NameR (stimStart),
  NameR (duration),
  NameR (binSize),
  NameR (spikeThre),
  NameR (alpha),
  NameR (beta),
};

real *initialCurrent;

int main(int argc, char **argv) {

  int ij;
  GetNameList (argc, argv);
  AllocArrays(0);
  InitList();
  SetParams();
  printf("max time steps = %f\n", tMax/deltaT);
  moreCycles = 1;
  while (moreCycles) {
    SingleStep();
    if (stepCount >= (tMax / deltaT)) moreCycles = 0;
  }
  PrintFiringRates();
  PrintRaster();
  AllocArrays(1);
}

void SingleStep() {
  int k;
  ++stepCount;
  timeNow = stepCount * deltaT;
  if ((timeNow >= stimStart) && (timeNow <= (duration + stimStart))) {
    DO_INH
      neti[k].stim = iMu + iSig*rand_gauss();//initialCurrent[k];//0.;
  } else {
    DO_INH
      neti[k].stim = 0.;//initialCurrent[k];
  }
  ComputeCurrents();
//  if(stepCount % stepAvg ==0) {
//  }
  PrintSummary(fopen("Istim.dat", "a+"));
}

void SetParams() {
  int k, l;
  oldTime = 0.;
  DO_INH {
    neti[k].v = -1.*(50.+20.*RandR());
    neti[k].m = AlphaM(vRest) / (AlphaM(vRest) + BethaM(vRest));
    neti[k].h = AlphaH(vRest) / (AlphaH(vRest) + BethaH(vRest));
    neti[k].n = AlphaN(vRest) / (AlphaN(vRest) + BethaN(vRest));
    initialCurrent[k] = iMu + iSig*rand_gauss();
    neti[k].isOn = 1;
  }
  printf("network setup done!\n");
}

void AllocArrays(int domem) {
  switch(domem) {
    case 0:
      AllocMem(neti, nIn, NeuronI);
      AllocMem(initialCurrent, nIn, real);
      break;
    case 1:
      free(neti);
      free(initialCurrent);
      break;
  }
}

void InitList() {
  int k, l;
//  DO_EXC {
//  }
  DO_INH {
    neti[k].spike.len = 0;
    neti[k].spike.index = NULL;
  }
}

void ComputeCurrents() {
  int k, l;
  real *k11, *k12, *k13, *k14, *h11, *h12, *h13, *h14, *n11, *n12, *n13, *n14;
  AllocMem(k11, nIn, real);
  AllocMem(k12, nIn, real);
  AllocMem(k13, nIn, real);
  AllocMem(k14, nIn, real);
  AllocMem(h11, nIn, real);
  AllocMem(h12, nIn, real);
  AllocMem(h13, nIn, real);
  AllocMem(h14, nIn, real);
  AllocMem(n11, nIn, real);
  AllocMem(n12, nIn, real);
  AllocMem(n13, nIn, real);
  AllocMem(n14, nIn, real);
  DO_INH {
    k11[k] = deltaT*KV(neti[k].stim,neti[k].v,neti[k].m,neti[k].h,neti[k].n);
    h11[k] = deltaT*Kh(neti[k].v,neti[k].h);
    n11[k] = deltaT*Kn(neti[k].v,neti[k].n);

    k12[k] = deltaT*KV(neti[k].stim,neti[k].v+(.5*k11[k]),neti[k].m,neti[k].h+(.5*h11[k]),neti[k].n+(.5*n11[k]));
    h12[k] = deltaT*Kh(neti[k].v+(.5*k11[k]),neti[k].h+(.5*h11[k]));
    n12[k] = deltaT*Kn(neti[k].v+(.5*k11[k]),neti[k].n+(.5*n11[k]));

    k13[k] = deltaT*KV(neti[k].stim,neti[k].v+(.5*k12[k]),neti[k].m,neti[k].h+(.5*h12[k]),neti[k].n+(.5*n12[k]));
    h13[k] = deltaT*Kh(neti[k].v+(.5*k12[k]),neti[k].h+(.5*h12[k]));
    n13[k] = deltaT*Kn(neti[k].v+(.5*k12[k]),neti[k].n+(.5*n12[k]));

    k14[k] = deltaT*KV(neti[k].stim,neti[k].v+k13[k],neti[k].m,neti[k].h+h13[k],neti[k].n+n13[k]);
    h14[k] = deltaT*Kh(neti[k].v+k13[k],neti[k].h+h13[k]);
    n14[k] = deltaT*Kn(neti[k].v+k13[k],neti[k].n+n13[k]);

    neti[k].m = AlphaM(neti[k].v) / (AlphaM(neti[k].v) + BethaM(neti[k].v));
    neti[k].v = neti[k].v + ((k11[k] + 2.*k12[k] + 2.*k13[k] + k14[k])/6.);
    neti[k].h = neti[k].h + ((h11[k] + 2.*h12[k] + 2.*h13[k] + h14[k])/6.);
    neti[k].n = neti[k].n + ((n11[k] + 2.*n12[k] + 2.*n13[k] + n14[k])/6.);

    if(neti[k].v>=spikeThre && neti[k].isOn==1) {
      AddList(&neti[k].spike, timeNow);
      neti[k].isOn = 0;
//      printf("%d ", k);
    }
    if(neti[k].v<spikeThre)
      neti[k].isOn = 1;

  }
  free(k11);
  free(k12);
  free(k13);
  free(k14);
  free(h11);
  free(h12);
  free(h13);
  free(h14);
  free(n11);
  free(n12);
  free(n13);
  free(n14);
}

real KV(real I, real V, real m, real h, real n) {
  real p;
  p = (I - ((gNaBar*(m*m*m)*h*(V-eNa)) + (gKBar*(n*n*n*n)*(V-eK)) + (gLBar*(V-eL)) ))/Capacity;
  return (p);
}

real Kh(real V, real h) {
  real p;
  p = phi * ((AlphaH(V) * (1. - h)) - (BethaH(V) * h));
  return (p);
}

real Kn(real V, real n) {
  real p;
  p = phi * ((AlphaN(V) * (1. - n)) - (BethaN(V) * n));
  return (p);
}

real Ks(real V, real s) {
  real p;
  p = alpha * ((F(V) * (1. - s)) - (beta * s));
  return (p);
}

void AddList(List *list,  real incre) {
  list->index=(real *)realloc(list->index, sizeof(real)*(list->len+1));
  list->index[list->len]=incre;
  list->len++;
}

void AddList2(List2 *list,  int incre) {
  list->index=(int *)realloc(list->index, sizeof(int)*(list->len+1));
  list->index[list->len]=incre;
  list->len++;
}

void PrintSummary(FILE *fp) {
  fprintf(fp, "%8.4f %7.4f %8.4f %8.4f %8.4f %8.4f\n", timeNow, neti[0].stim, neti[0].v, neti[0].m, neti[0].n, neti[0].h);
  fflush(fp);
  fclose(fp);
}

void Coherence(int ht) {
  int k, totBins, i, j, pairNums, b, counter, c, dumCount;
  real l, *kappa, numen, denum1, denum2;
  typedef struct {
    real ind[(long) ((tMax-100.)/tau)];
  } train;
  train *spTrain;
  totBins = tMax/tau;
  pairNums = nIn*(nIn-1)/2;
  printf("\ncalculating the coherence measure...\n");
  AllocMem(spTrain, nIn, train);
  DO_INH {
    counter = 0;
    for(l=tMax-100.; l<tMax; l+=tau) {
      for(b=0; b<neti[k].spike.len; b++) {
	spTrain[k].ind[counter] = 0.;
	if(neti[k].spike.index[b]>=l && neti[k].spike.index[b]<=(l+tau)) {
	  spTrain[k].ind[counter] = 1.;
	  break;
	}
      }
      counter += 1;
    }
  }
  AllocMem(kappa, pairNums, real);
  dumCount = 0;
  char buff[30];
  sprintf(buff, "%s_%05d", "coherence", ht);
  strcat(buff, ".dat");
  FILE *flo;
  flo = fopen(buff, "w+");
  for(i=0; i<(nIn-1); i++) {
    for(j=(i+1); j<nIn; j++) {
      numen = 0.;
      denum1 = 0.;
      denum2 = 0.;
      for(c=0; c<((long)((tMax-100.)/tau)); c++) {
	numen += spTrain[i].ind[c]*spTrain[j].ind[c];
	denum1 += spTrain[i].ind[c];
	denum2 += spTrain[j].ind[c];
      }
      kappa[dumCount] = numen/sqrt(denum1*denum2);
      if(!isnan(kappa[dumCount]))
	fprintf(flo, "%g\n", kappa[dumCount]);
      dumCount += 1;
    }
  }
  fclose(flo);
  free(spTrain);
  free(kappa);
}

void PrintFiringRates() {
  int k, i;
  FILE * fll;
  fll = fopen("FR.dat", "w+");
  DO_INH {
    fprintf(fll, "%g\n", (neti[k].spike.len/tMax)*1000.);
  }
  fclose(fll);
}

void PrintRaster() {
  int k, i;
  FILE *flf;
  flf = fopen("V1.dat", "w+");
  DO_INH {
    for(i=0; i<neti[k].spike.len; i++)
      fprintf(flf, "%d %g\n", k, neti[k].spike.index[i]);
  }
  fclose(flf);
}

#include "in_namelist.c"
#include "in_rand.c"
