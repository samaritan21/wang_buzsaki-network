#!/bin/bash

unamestr=`uname`

rm Istim.dat hodgkin lfp.dat coherence* FR.dat V1.dat V21.dat indexes1.dat stimInd.dat times1*
if [[ "$unamestr" == 'Linux' ]]; then
        gcc -o hodgkin hodgkin.c -lm
elif [[ "$unamestr" == 'Darwin' ]]; then
        gcc-8 -o hodgkin hodgkin.c -lm
fi

./hodgkin

gnuplot << EOG
set term pngcairo size 800,600 enhanced
set output 'sample.png'
set title "sample cell"
set ytics nomirror
set y2tics nomirror
set yrange [-100:60]
set y2range [0:35]
set xlabel "time [ms]"
set ylabel "membrain potential [mv]"
set y2label "Iext [nA]"
plot 'Istim.dat' u 1:8 w l title 'membrain potential' axes x1y1, 'Istim.dat' u 1:2 w l title 'external input' axes x1y2
EOG

gnuplot << EOG
set term pngcairo size 800,600 enhanced
set output 'synaptic_field.png'
set ytics nomirror
set yrange [0:35]
set xlabel "time [ms]"
set ylabel "s"
plot "lfp.dat" u 1:2 w l title 'LFP' lc rgb 'black'
EOG


counter=0
for file in coherence_*.dat
do
echo "$counter `awk '{sum+=$1} END {print sum/NR}' $file`" >> coherenceAvg.dat
#echo "average coherence: `awk '{sum+=$1} END {print sum/NR}' $file`" >> coherenceAvg.dat
echo "`awk '{sum+=$1; sumsq+=$1*$1} END {print sqrt(sumsq/NR - (sum/NR)**2)}' $file`" >> coherenceStd.dat
#echo "std of coherence: `awk '{sum+=$1; sumsq+=$1*$1} END {print sqrt(sumsq/NR - (sum/NR)**2)}' $file`" >> coherenceStd.dat
let counter+=1
done

paste coherenceAvg.dat coherenceStd.dat > tempo.dat
echo "`awk '{print $2-$3}' tempo.dat`" > yMin.dat
echo "`awk '{print $2+$3}' tempo.dat`" > yMax.dat
paste coherenceAvg.dat yMin.dat yMax.dat > temp.dat
rm tempo.dat yMin.dat yMax.dat
gnuplot << EOG
set term pngcairo size 800,600 enhanced
set output 'coherence.png'
set xlabel "{/Symbol t}"
set ylabel "{/Symbol k}"
set style fill transparent solid 0.1 noborder
plot 'temp.dat' u 1:3:4 w filledcurves notitle, '' u 1:2 with lp lt 1 pt 7 ps 1.5 lw 3 notitle
EOG
rm temp.dat

gnuplot << EOG
set term pngcairo size 800,600 enhanced
set output 'raster.png'
set xlabel "time [ms]"
set ylabel "neuron ID"
p 'V1.dat' u 2:1 w p pt 7 ps .2 lc rgb "black" notitle
EOG

echo "Average firing rate: `awk '{sum+=$1} END {print sum/NR}' FR.dat`"
echo "STD of firing rate: `awk '{sum+=$1; sumsq+=$1*$1} END {print sqrt(sumsq/NR - (sum/NR)**2)}' FR.dat`"

python3 spectrum.py

if [[ "$unamestr" == 'Linux' ]]; then
        eog sample.png
	eog synaptic_field.png
	eog coherence.png
	eog raster.png
        eog spectrum.png
elif [[ "$unamestr" == 'Darwin' ]]; then
        open sample.png
	open synaptic_field.png
        open coherence.png
        open raster.png
        open spectrum.png
fi
