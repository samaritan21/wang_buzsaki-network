
#define IADD   453806245
#define IMUL   314159269
#define MASK   2147483647
#define SCALE  0.4656612873e-9

int randSeedP = 17;

void InitRand (int randSeedI)
{
  struct timeval tv;

  if (randSeedI != 0) randSeedP = randSeedI;
  else {
    gettimeofday (&tv, 0);
    randSeedP = tv.tv_usec;
  }
}

real RandR ()
{
  randSeedP = (randSeedP * IMUL + IADD) & MASK;
  return (randSeedP * SCALE);
}

void VRand (VecR *p)
{
//real s;

//  s = 2. * M_PI * RandR ();
  p->x = RandR();//.5*(1+cos (s));
  p->y = RandR();//.5*(1+sin (s));
}

int InRand (int f)
{
//  real B;

//  B = 2. * M_PI * RandR ();
  return (rand() % f);
}

real rand_gauss(void) {
  real v1, v2, s;

  do {
    v1 = 2.0 * ((real) rand()/RAND_MAX) - 1;
    v2 = 2.0 * ((real) rand()/RAND_MAX) - 1;
    s = v1*v1 + v2*v2;
  } while(s >= 1.);

  if(s==0.)
    return 0.;
  else
    return (v1*sqrt(-2.*log(s)/s));
}

real exp1(real x) {
  x = 1.0 + x / 4096.0;
  x *= x; x *= x; x *= x; x *= x;
  x *= x; x *= x; x *= x; x *= x;
  x *= x; x *= x; x *= x; x *= x;
  return x;
}
